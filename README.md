# Hello there 👋🏾

Welcome to my profile, I'm glad you're here.

- Direction science since 2017 🏹
- Currently studying 🎓

I love depth of art and different forms of expression, playing with Gentoo and getting stuff done with Vim.

## Education 🧠

- Computer science highschool and university 🏛️
- Openness to new views and strive for knowlage 🤓

## Skills 👾

> hardware

> software

> math

## Fields of interest 🗺️

- The bigger picture of technology integration in the future 🖥️
- Writing ☕

## Contact 📫

